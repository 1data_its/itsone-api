<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tabel1_model extends CI_Model {

    public function getData()
    {
        // return "hash";     
        return $this->db->get('tabel1')->result_array();
    }

    public function deleteData($id)
    {
        $this->db->delete('tabel1', ['id' => $id]);
        return $this->db->affected_rows();
    }

    public function createData($data)
    {
        $this->db->insert('tabel1', $data);
        return $this->db->insert_id();
    }

    public function updateData($data, $id)
    {
        
        $this->db->update('tabel1', $data, ['id' => $id]);
        return $this->db->affected_rows();
    }

}

/* End of file Tabel1_model.php */
/* Location: ./application/models/Tabel1_model.php */