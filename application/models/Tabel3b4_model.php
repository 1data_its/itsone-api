<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tabel3b4_model extends CI_Model {

    public function getData()
    {
        return $this->db->get('tabel3b4')->result_array();
    }

    public function deleteData($id)
    {
        $this->db->delete('tabel3b4', ['id' => $id]);
        return $this->db->affected_rows();
    }

    public function createData($data)
    {
        $this->db->insert('tabel3b4', $data);
        return $this->db->insert_id();
    }

    public function updateData($data, $id)
    {
        
        $this->db->update('tabel3b4', $data, ['id' => $id]);
        return $this->db->affected_rows();
    }

}

/* End of file tabel3b4_model.php */
/* Location: ./application/models/Tabel1_model.php */