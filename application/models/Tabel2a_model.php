<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tabel2a_model extends CI_Model {

    public function getData()
    {
        return $this->db->get('tabel2a')->result_array();
    }

    public function deleteData($id)
    {
        $this->db->delete('tabel2a', ['id' => $id]);
        return $this->db->affected_rows();
    }

    public function createData($data)
    {
        $this->db->insert('tabel2a', $data);
        return $this->db->insert_id();
    }

    public function updateData($data, $id)
    {
        
        $this->db->update('tabel2a', $data, ['id' => $id]);
        return $this->db->affected_rows();
    }

}

/* End of file tabel2a_model.php */
/* Location: ./application/models/Tabel1_model.php */