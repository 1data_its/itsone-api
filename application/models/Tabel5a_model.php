<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tabel5a_model extends CI_Model {

    public function getData()
    {
        // return "hash";     
        return $this->db->get('tabel5a')->result_array();
    }

    public function deleteData($id)
    {
        $this->db->delete('tabel5a', ['id' => $id]);
        return $this->db->affected_rows();
    }

    public function createData($data)
    {
        $this->db->insert('tabel5a', $data);
        return $this->db->insert_id();
    }

    public function updateData($data, $id)
    {
        
        $this->db->update('tabel5a', $data, ['id' => $id]);
        return $this->db->affected_rows();
    }

}

/* End of file Tabel5a_model.php */
/* Location: ./application/models/Tabel5a_model.php */