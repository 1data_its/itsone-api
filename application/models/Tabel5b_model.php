<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tabel5b_model extends CI_Model {

    public function getData()
    {
        // return "hash";     
        return $this->db->get('tabel5b')->result_array();
    }

    public function deleteData($id)
    {
        $this->db->delete('tabel5b', ['id' => $id]);
        return $this->db->affected_rows();
    }

    public function createData($data)
    {
        $this->db->insert('tabel5b', $data);
        return $this->db->insert_id();
    }

    public function updateData($data, $id)
    {
        
        $this->db->update('tabel5b', $data, ['id' => $id]);
        return $this->db->affected_rows();
    }

}

/* End of file Tabel5b_model.php */
/* Location: ./application/models/Tabel5b_model.php */