<?php

class Kuliahtamu_model extends CI_Model
{
    public function getData()
    {
        return $this->db->get('kuliahtamu')->result_array();
    }

    public function deleteData($id)
    {
        $this->db->delete('kuliahtamu', ['id' => $id]);
        return $this->db->affected_rows();
    }

    public function createData($data)
    {
        $this->db->insert('kuliahtamu', $data);
        return $this->db->insert_id();
    }

    public function updateData($data, $id)
    {
        
        $this->db->update('kuliahtamu', $data, ['id' => $id]);
        return $this->db->affected_rows();
    }
}