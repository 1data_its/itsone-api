<?php

class Tabel8b2_model extends CI_Model
{
    public function getData()
    {
        return $this->db->get('tabel8b2')->result_array();
    }

    public function deleteData($id)
    {
        $this->db->delete('tabel8b2', ['id' => $id]);
        return $this->db->affected_rows();
    }

    public function createData($data)
    {
        $this->db->insert('tabel8b2', $data);
        return $this->db->insert_id();
    }

    public function updateData($data, $id)
    {
        $this->db->update('tabel8b2', $data, ['id' => $id]);
        return $this->db->affected_rows();
    }
}