<?php

class Login_model extends CI_Model
{
    public function gettoken($username, $password)
    {
        return $this->db->get_where('keys', ['username' => $username, 'password' => $password])->result_array();
    }

    public function deleteuser($id)
    {
        $this->db->delete('keys', ['id' => $id]);
        return $this->db->affected_rows();
    }

    public function createuser($data)
    {
        $this->db->insert('keys', $data);
        return $this->db->affected_rows();
    }

    public function updateuser($data, $id)
    {
        $this->db->update('keys', $data, ['id' => $id]);
        return $this->db->affected_rows();
    }
}