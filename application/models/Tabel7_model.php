<?php

class Tabel7_model extends CI_Model
{
    public function getData()
    {
        return $this->db->get('tabel7')->result_array();
    }

    public function deleteData($id)
    {
        $this->db->delete('tabel7', ['id' => $id]);
        return $this->db->affected_rows();
    }

    public function createData($data)
    {
        $this->db->insert('tabel7', $data);
        return $this->db->insert_id();
    }

    public function updateData($data, $id)
    {
        
        $this->db->update('tabel7', $data, ['id' => $id]);
        return $this->db->affected_rows();
    }
}