<?php

class Matkulbing_model extends CI_Model
{
    public function getData()
    {
        return $this->db->get('matkulbing')->result_array();
    }

    public function deleteData($id)
    {
        $this->db->delete('matkulbing', ['id' => $id]);
        return $this->db->affected_rows();
    }

    public function createData($data)
    {
        $this->db->insert('matkulbing', $data);
        return $this->db->insert_id();
    }

    public function updateData($data, $id)
    {
        $this->db->update('matkulbing', $data, ['id' => $id]);
        return $this->db->affected_rows();
    }
}