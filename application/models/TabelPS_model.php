<?php

class TabelPS_model extends CI_Model
{
    public function getData()
    {
        return $this->db->get('tabelps')->result_array();
    }

    public function deleteData($id)
    {
        $this->db->delete('tabelps', ['id' => $id]);
        return $this->db->affected_rows();
    }

    public function createData($data)
    {
        $this->db->insert('tabelps', $data);
        return $this->db->insert_id();
    }

    public function updateData($data, $id)
    {
        
        $this->db->update('tabelps', $data, ['id' => $id]);
        return $this->db->affected_rows();
    }
}