<?php

class Tabel8e_1_model extends CI_Model
{
    public function getData()
    {
        return $this->db->get('tabel8e_1')->result_array();
    }

    public function deleteData($id)
    {
        $this->db->delete('tabel8e_1', ['id' => $id]);
        return $this->db->affected_rows();
    }

    public function createData($data)
    {
        $this->db->insert('tabel8e_1', $data);
        return $this->db->insert_id();
    }

    public function updateData($data, $id)
    {
        $this->db->update('tabel8e_1', $data, ['id' => $id]);
        return $this->db->affected_rows();
    }
}