<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tabel4_model extends CI_Model {

    public function getData()
    {
        // return "hash";     
        return $this->db->get('tabel4')->result_array();
    }

    public function deleteData($id)
    {
        $this->db->delete('tabel4', ['id' => $id]);
        return $this->db->affected_rows();
    }

    public function createData($data)
    {
        $this->db->insert('tabel4', $data);
        return $this->db->insert_id();
    }

    public function updateData($data, $id)
    {
        
        $this->db->update('tabel4', $data, ['id' => $id]);
        return $this->db->affected_rows();
    }

}

/* End of file Tabel4_model.php */
/* Location: ./application/models/Tabel1_model.php */