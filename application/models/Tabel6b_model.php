<?php

class Tabel6b_model extends CI_Model
{
    public function getData()
    {
        return $this->db->get('tabel6b')->result_array();
    }

    public function deleteData($id)
    {
        $this->db->delete('tabel6b', ['id' => $id]);
        return $this->db->affected_rows();
    }

    public function createData($data)
    {
        $this->db->insert('tabel6b', $data);
        return $this->db->insert_id();
    }

    public function updateData($data, $id)
    {
        
        $this->db->update('tabel6b', $data, ['id' => $id]);
        return $this->db->affected_rows();
    }
}