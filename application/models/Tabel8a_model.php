<?php

class Tabel8a_model extends CI_Model
{
    public function getData()
    {
        return $this->db->get('tabel8a')->result_array();
    }

    public function deleteData($id)
    {
        $this->db->delete('tabel8a', ['id' => $id]);
        return $this->db->affected_rows();
    }

    public function createData($data)
    {
        $this->db->insert('tabel8a', $data);
        return $this->db->insert_id();
    }

    public function updateData($data, $id)
    {
        
        $this->db->update('tabel8a', $data, ['id' => $id]);
        return $this->db->affected_rows();
    }
}