<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tabel3b3_model extends CI_Model {

    public function getData()
    {
        return $this->db->get('tabel3b3')->result_array();
    }

    public function deleteData($id)
    {
        $this->db->delete('tabel3b3', ['id' => $id]);
        return $this->db->affected_rows();
    }

    public function createData($data)
    {
        $this->db->insert('tabel3b3', $data);
        return $this->db->insert_id();
    }

    public function updateData($data, $id)
    {
        
        $this->db->update('tabel3b3', $data, ['id' => $id]);
        return $this->db->affected_rows();
    }

}

/* End of file tabel3b3_model.php */
/* Location: ./application/models/Tabel1_model.php */