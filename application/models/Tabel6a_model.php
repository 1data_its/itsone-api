<?php

class Tabel6a_model extends CI_Model
{
    public function getData()
    {
        return $this->db->get('tabel6a')->result_array();
    }

    public function deleteData($id)
    {
        $this->db->delete('tabel6a', ['id' => $id]);
        return $this->db->affected_rows();
    }

    public function createData($data)
    {
        $this->db->insert('tabel6a', $data);
        return $this->db->insert_id();
    }

    public function updateData($data, $id)
    {
        
        $this->db->update('tabel6a', $data, ['id' => $id]);
        return $this->db->affected_rows();
    }
}