<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tabel5c_model extends CI_Model {

    public function getData()
    {
        // return "hash";     
        return $this->db->get('tabel5c')->result_array();
    }

    public function deleteData($id)
    {
        $this->db->delete('tabel5c', ['id' => $id]);
        return $this->db->affected_rows();
    }

    public function createData($data)
    {
        $this->db->insert('tabel5c', $data);
        return $this->db->insert_id();
    }

    public function updateData($data, $id)
    {
        
        $this->db->update('tabel5c', $data, ['id' => $id]);
        return $this->db->affected_rows();
    }

}

/* End of file Tabel5c_model.php */
/* Location: ./application/models/Tabel5c_model.php */