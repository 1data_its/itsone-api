<?php 

use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Tabel3a3 extends CI_Controller
{

    use REST_Controller {
        REST_Controller::__construct as private __resTraitConstruct;
    }
    
    public function __construct()
    {
        parent::__construct();
        $this->__resTraitConstruct();
        $this->load->model('Tabel3a3_model', 'tb');
    }
    
    public function index_get()
    {
        $st = $this->tb->getData();
        if($st)
        {
            $this->response([
                'status' => true,
                'data' => $st
            ], 200);
        }
        else
        {
            $this->response([
                'status' => false,
                'message' => 'tidak ditemukan data'
            ], 404);
        }
    }

    public function  index_delete()
    {
        $id = $this->input->get('id');

        if($id == null)
        {
            $this->response([
                'status' => false,
                'message' => $id
            ], 200);
        }
        else
        {
            if($this->tb->deleteData($id) > 0)
            {
                $this->response([
                    'status' => true,
                    'id' => $id,
                    'message' => 'deleted'
                ], 200);
            }
            else
            {
                $this->response([
                    'status' => false,
                    'message' => 'id tidak ditemukan'
                ], 404);
            }
        }
    }
    
    public function  index_post()
    {
        $data = [
            'prodi' => $this->post('prodi'),
            'namaDosen' => $this->post('namaDosen'),
            'pendidikan' => $this->post('pendidikan'),
            'keahlian' => $this->post('keahlian'),
            'jabatan' => $this->post('jabatan'),
            'sertifikatPendidik' => $this->post('sertifikatPendidik'),
            'sertifikatKompetensi' => $this->post('sertifikatKompetensi'),
            'matkulDiampu' => $this->post('matkulDiampu'),
            'kesesuaianKeahlian' => $this->post('kesesuaianKeahlian'),
        ];
        
        $req =  $this->tb->createData($data);
        if( $req > 0)
        {
            $this->response([
                'status' => true,
                'id'=> $req,
                'message' => 'data baru telah ditambahkan'
            ], 201);
        }
        else 
        {
            $this->response([
                'status' => false,
                'message' => 'post failed'
            ], 400);
        }
    }

    public function  index_put()
    {
        $id = $this->put('id');
        $data = [
            'prodi' => $this->put('prodi'),
            'namaDosen' => $this->put('namaDosen'),
            'pendidikan' => $this->put('pendidikan'),
            'keahlian' => $this->put('keahlian'),
            'jabatan' => $this->put('jabatan'),
            'sertifikatPendidik' => $this->put('sertifikatPendidik'),
            'sertifikatKompetensi' => $this->put('sertifikatKompetensi'),
            'matkulDiampu' => $this->put('matkulDiampu'),
            'kesesuaianKeahlian' => $this->put('kesesuaianKeahlian'),
        ];
        
        if( $this->tb->updateData($data, $id) > 0)
        {
            $this->response([
                'status' => true,
                'id' =>$id,
                'message' => 'data has been updated'
            ], 200);
        }
        else 
        {
            $this->response([
                'status' => false,
                'id' =>$id,
                'message' => 'update nothing'
            ], 200);
        }
    }
}

