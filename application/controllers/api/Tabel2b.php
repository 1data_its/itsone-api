<?php 

use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Tabel2b extends CI_Controller
{

    use REST_Controller {
        REST_Controller::__construct as private __resTraitConstruct;
    }
    
    public function __construct()
    {
        parent::__construct();
        $this->__resTraitConstruct();
        $this->load->model('Tabel2b_model', 'tb');
    }
    
    public function index_get()
    {
        $st = $this->tb->getData();
        if($st)
        {
            $this->response([
                'status' => true,
                'data' => $st
            ], 200);
        }
        else
        {
            $this->response([
                'status' => false,
                'message' => 'tidak ditemukan data',
                'data' => $st
            ], 404);
        }
    }

    public function  index_delete()
    {
        $id = $this->input->get('id');
        if($id == null)
        {
            $this->response([
                'status' => false,
                'message' => $id
            ], 200);
        }
        else
        {
            if($this->tb->deleteData($id) > 0)
            {
                $this->response([
                    'status' => true,
                    'id' => $id,
                    'message' => 'deleted'
                ], 200);
            }
            else
            {
                $this->response([
                    'status' => false,
                    'message' => 'id tidak ditemukan'
                ], 404);
            }
        }
    }
    
    public function  index_post()
    {
        // var_dump($this->post()); return;
        $data = [
            'prodi' => $this->post('prodi'),
            'aktifTS2' => $this->post('aktifTS2'),
            'aktifTS1' => $this->post('aktifTS1'),
            'aktifTS' => $this->post('aktifTS'),
            'AsingPenuhTS2' => $this->post('AsingPenuhTS2'),
            'AsingPenuhTS1' => $this->post('AsingPenuhTS1'),
            'AsingPenuhTS' => $this->post('AsingPenuhTS'),
            'AsingParuhTS2' => $this->post('AsingParuhTS2'),
            'AsingParuhTS1' => $this->post('AsingParuhTS1'),
            'AsingParuhTS' => $this->post('AsingParuhTS')
        ];
        
        $req =  $this->tb->createData($data);
        if( $req > 0)
        {
            $this->response([
                'status' => true,
                'id'=> $req,
                'message' => 'data baru telah ditambahkan'
            ], 201);
        }
        else 
        {
            $this->response([
                'status' => false,
                'message' => 'post failed'
            ], 400);
        }
    }

    public function  index_put()
    {
        $id = $this->put('id');
        $data = [
            'prodi' => $this->put('prodi'),
            'aktifTS2' => $this->put('aktifTS2'),
            'aktifTS1' => $this->put('aktifTS1'),
            'aktifTS' => $this->put('aktifTS'),
            'AsingPenuhTS2' => $this->put('AsingPenuhTS2'),
            'AsingPenuhTS1' => $this->put('AsingPenuhTS1'),
            'AsingPenuhTS' => $this->put('AsingPenuhTS'),
            'AsingParuhTS2' => $this->put('AsingParuhTS2'),
            'AsingParuhTS1' => $this->put('AsingParuhTS1'),
            'AsingParuhTS' => $this->put('AsingParuhTS'),
        ];
        
        if( $this->tb->updateData($data, $id) > 0)
        {
            $this->response([
                'status' => true,
                'id' =>$id,
                'message' => 'data has been updated'
            ], 200);
        }
        else 
        {
            $this->response([
                'status' => false,
                'id' =>$id,
                'message' => 'update nothing'
            ], 200);
        }
    }
}

