<?php 

use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Login extends CI_Controller
{

    use REST_Controller {
        REST_Controller::__construct as private __resTraitConstruct;
    }
    
    public function __construct()
    {
        parent::__construct();
        $this->__resTraitConstruct();
        $this->load->model('Login_model', 'login');
    }
    
    public function index_get()
    {
        $username = $this->get('username');
        $password = md5($this->get('password'));

        $token = $this->login->gettoken($username, $password);

        if($token)
        {
            $this->response([
                'status' => true,
                'data' => $token[0]['key']
            ], 200);
        }
        else
        {
            $this->response([
                'status' => false,
                'message' => 'username atau password salah'
            ], 404);
        }
    }

    public function  index_delete()
    {
        $id = $this->delete('id');
        if($id === null)
        {
            $this->response([
                'status' => false,
                'message' => 'input id'
            ], 400);
        }
        else
        {
            if($this->login->deleteuser($id) > 0)
            {
                $this->response([
                    'status' => true,
                    'id' => $id,
                    'message' => 'deleted'
                ], 200);
            }
            else
            {
                $this->response([
                    'status' => false,
                    'message' => 'id tidak ditemukan'
                ], 404);
            }
        }
    }
    
    public function  index_post()
    {
        $data = [
            'username' => $this->post('username'),
            'password' => md5($this->post('password')),
            'key' => md5($this->post('password'))
        ];

        if( $this->login->createuser($data) > 0)
        {
            $this->response([
                'status' => true,
                'message' => 'new user has been posted'
            ], 201);
        }
        else 
        {
            $this->response([
                'status' => false,
                'message' => 'post failed'
            ], 400);
        }
    }

    public function  index_put()
    {
        $id = $this->put('id');

        $data = [
            'username' => $this->put('username'),
            'password' => md5($this->put('password')),
            'key' => md5($this->put('password'))
        ];
        
        if( $this->login->updateuser($data, $id) > 0)
        {
            $this->response([
                'status' => true,
                'id' =>$id,
                'message' => 'user has been updated'
            ], 200);
        }
        else
        {
            $this->response([
                'status' => false,
                'message' => 'update failed'
            ], 400);
        }
    }

}

