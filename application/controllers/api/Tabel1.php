<?php 

use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Tabel1 extends CI_Controller
{

    use REST_Controller {
        REST_Controller::__construct as private __resTraitConstruct;
    }
    
    public function __construct()
    {
        parent::__construct();
        $this->__resTraitConstruct();
        $this->load->model('Tabel1_model', 'tb');
    }
    
    public function index_get()
    {
        $st = $this->tb->getData();
        if($st)
        {
            $this->response([
                'status' => true,
                'data' => $st
            ], 200);
        }
        else
        {
            $this->response([
                'status' => false,
                'message' => 'tidak ditemukan data',
                'data' => $st
            ], 404);
        }
    }

    public function  index_delete()
    {
        $id = $this->input->get('id');
        if($id == null)
        {
            $this->response([
                'status' => false,
                'message' => $id
            ], 200);
        }
        else
        {
            if($this->tb->deleteData($id) > 0)
            {
                $this->response([
                    'status' => true,
                    'id' => $id,
                    'message' => 'deleted'
                ], 200);
            }
            else
            {
                $this->response([
                    'status' => false,
                    'message' => 'id tidak ditemukan'
                ], 404);
            }
        }
    }
    
    public function  index_post()
    {
        // var_dump($this->post()); return;
        $data = [
            'prodi' => $this->post('prodi'),
            'lembagaMitra' => $this->post('lembagaMitra'),
            'tInter' => $this->post('tInter'),
            'tNasional' => $this->post('tNasional'),
            'tLokal' => $this->post('tLokal'),
            'judulKegiatan' => $this->post('judulKegiatan'),
            'manfaat' => $this->post('manfaat'),
            'waktu' => $this->post('waktu'),
            'bukti' => $this->post('bukti'),
            'tahun' => $this->post('tahun')
        ];
        
        $req =  $this->tb->createData($data);
        if( $req > 0)
        {
            $this->response([
                'status' => true,
                'id'=> $req,
                'message' => 'data baru telah ditambahkan'
            ], 201);
        }
        else 
        {
            $this->response([
                'status' => false,
                'message' => 'post failed'
            ], 400);
        }
    }

    public function  index_put()
    {
        $id = $this->put('id');
        $data = [
            'prodi' => $this->put('prodi'),
            'lembagaMitra' => $this->put('lembagaMitra'),
            'tInter' => $this->put('tInter'),
            'tNasional' => $this->put('tNasional'),
            'tLokal' => $this->put('tLokal'),
            'judulKegiatan' => $this->put('judulKegiatan'),
            'manfaat' => $this->put('manfaat'),
            'waktu' => $this->put('waktu'),
            'bukti' => $this->put('bukti'),
            'tahun' => $this->put('tahun')
        ];
        
        if( $this->tb->updateData($data, $id) > 0)
        {
            $this->response([
                'status' => true,
                'id' =>$id,
                'message' => 'data has been updated'
            ], 200);
        }
        else 
        {
            $this->response([
                'status' => false,
                'id' =>$id,
                'message' => 'update nothing'
            ], 200);
        }
    }
}

