<?php 

use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Tabel4 extends CI_Controller
{

    use REST_Controller {
        REST_Controller::__construct as private __resTraitConstruct;
    }
    
    public function __construct()
    {
        parent::__construct();
        $this->__resTraitConstruct();
        $this->load->model('Tabel4_model', 'tb');
    }
    
    public function index_get()
    {
        $st = $this->tb->getData();
        if($st)
        {
            $this->response([
                'status' => true,
                'data' => $st
            ], 200);
        }
        else
        {
            $this->response([
                'status' => false,
                'message' => 'tidak ditemukan data',
                'data' => $st
            ], 404);
        }
    }

    public function  index_delete()
    {
        $id = $this->input->get('id');
        if($id == null)
        {
            $this->response([
                'status' => false,
                'message' => $id
            ], 200);
        }
        else
        {
            if($this->tb->deleteData($id) > 0)
            {
                $this->response([
                    'status' => true,
                    'id' => $id,
                    'message' => 'deleted'
                ], 200);
            }
            else
            {
                $this->response([
                    'status' => false,
                    'message' => 'id tidak ditemukan'
                ], 404);
            }
        }
    }
    
    public function  index_post()
    {
        // var_dump($this->post()); return;
        $data = [
            'prodi' => $this->post('prodi'),
            'jenisPenggunaan' => $this->post('jenisPenggunaan'),
            'uppsTS2' => $this->post('uppsTS2'),
            'uppsTS1' => $this->post('uppsTS1'),
            'uppsTS' => $this->post('uppsTS'),
            'uppsRata2' => $this->post('uppsRata2'),
            'psTS2' => $this->post('psTS2'),
            'psTS1' => $this->post('psTS1'),
            'psTS' => $this->post('psTS'),
            'psRata2' => $this->post('psRata2'),
        ];
        
        $req =  $this->tb->createData($data);
        if( $req > 0)
        {
            $this->response([
                'status' => true,
                'id'=> $req,
                'message' => 'data baru telah ditambahkan'
            ], 201);
        }
        else 
        {
            $this->response([
                'status' => false,
                'message' => 'post failed'
            ], 400);
        }
    }

    public function  index_put()
    {
        $id = $this->put('id');
        $data = [
            'prodi' => $this->put('prodi'),
            'jenisPenggunaan' => $this->put('jenisPenggunaan'),
            'uppsTS2' => $this->put('uppsTS2'),
            'uppsTS1' => $this->put('uppsTS1'),
            'uppsTS' => $this->put('uppsTS'),
            'uppsRata2' => $this->put('uppsRata2'),
            'psTS2' => $this->put('psTS2'),
            'psTS1' => $this->put('psTS1'),
            'psTS' => $this->put('psTS'),
            'psRata2' => $this->put('psRata2'),
        ];
        
        if( $this->tb->updateData($data, $id) > 0)
        {
            $this->response([
                'status' => true,
                'id' =>$id,
                'message' => 'data has been updated'
            ], 200);
        }
        else 
        {
            $this->response([
                'status' => false,
                'id' =>$id,
                'message' => 'update nothing'
            ], 200);
        }
    }
}

