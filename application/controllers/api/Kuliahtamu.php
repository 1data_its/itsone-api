<?php 

use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Kuliahtamu extends CI_Controller
{

    use REST_Controller {
        REST_Controller::__construct as private __resTraitConstruct;
    }
    
    public function __construct()
    {
        parent::__construct();
        $this->__resTraitConstruct();
        $this->load->model('Kuliahtamu_model', 'tb');
    }
    
    public function index_get()
    {
        $request = $this->tb->getData();
        if($request)
        {
            $this->response([
                'status' => true,
                'data' => $request
            ], 200);
        }
        else
        {
            $this->response([
                'status' => false,
                'message' => 'tidak ditemukan data'
            ], 404);
        }
    }

    public function  index_delete()
    {
        $id = $this->input->get('id');

        if($id == null)
        {
            $this->response([
                'status' => false,
                'message' => 'Masukkan id'
            ], 400);
        }
        else
        {
            if($this->tb->deleteData($id) > 0)
            {
                $this->response([
                    'status' => true,
                    'id' => $id,
                    'message' => 'deleted'
                ], 200);
            }
            else
            {
                $this->response([
                    'status' => false,
                    'message' => 'id tidak ditemukan'
                ], 404);
            }
        }
    }
    
    public function  index_post()
    {
        $data = [
            'departemen' => $this->post('departemen'),
            'tanggal' => $this->post('tanggal'),
            'pemateri' => $this->post('pemateri'),
            'asal' => $this->post('asal'),
            'judulTopik' => $this->post('judulTopik'),
            'tingkat' => $this->post('tingkat'),
            'prodi' => $this->post('prodi')
        ];
        
        $request =  $this->tb->createData($data);
        if( $request > 0)
        {
            $this->response([
                'status' => true,
                'id'=> $request,
                'message' => 'data baru telah ditambahkan'
            ], 201);
        }
        else 
        {
            $this->response([
                'status' => false,
                'message' => 'post gagal'
            ], 400);
        }
    }

    public function  index_put()
    {
        $id = $this->put('id');
        $data = [
            'departemen' => $this->put('departemen'),
            'tanggal' => $this->put('tanggal'),
            'pemateri' => $this->put('pemateri'),
            'asal' => $this->put('asal'),
            'judulTopik' => $this->put('judulTopik'),
            'tingkat' => $this->put('tingkat'),
            'prodi' => $this->put('prodi')
        ];
        
        if( $this->tb->updateData($data, $id) > 0)
        {
            $this->response([
                'status' => true,
                'id' =>$id,
                'message' => 'data telah diperbaharui'
            ], 200);
        }
        else
        {
            $this->response([
                'status' => false,
                'id' =>$id,
                'message' => 'update gagal'
            ], 200);
        }
    }
}

