<?php 

use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Tabel3a2 extends CI_Controller
{

    use REST_Controller {
        REST_Controller::__construct as private __resTraitConstruct;
    }
    
    public function __construct()
    {
        parent::__construct();
        $this->__resTraitConstruct();
        $this->load->model('Tabel3a2_model', 'tb');
    }
    
    public function index_get()
    {
        $st = $this->tb->getData();
        if($st)
        {
            $this->response([
                'status' => true,
                'data' => $st
            ], 200);
        }
        else
        {
            $this->response([
                'status' => false,
                'message' => 'tidak ditemukan data'
            ], 404);
        }
    }

    public function  index_delete()
    {
        $id = $this->input->get('id');

        if($id == null)
        {
            $this->response([
                'status' => false,
                'message' => $id
            ], 200);
        }
        else
        {
            if($this->tb->deleteData($id) > 0)
            {
                $this->response([
                    'status' => true,
                    'id' => $id,
                    'message' => 'deleted'
                ], 200);
            }
            else
            {
                $this->response([
                    'status' => false,
                    'message' => 'id tidak ditemukan'
                ], 404);
            }
        }
    }
    
    public function  index_post()
    {
        $data = [
            'prodi' => $this->post('prodi'),
            'namaDosen' => $this->post('namaDosen'),
            'DTPS' => $this->post('DTPS'),
            'PSAkreditasi' => $this->post('PSAkreditasi'),
            'PSLainDalamPT' => $this->post('PSLainDalamPT'),
            'PSLainLuarPT' => $this->post('PSLainLuarPT'),
            'penelitian' => $this->post('penelitian'),
            'pkm' => $this->post('pkm'),
            'tugasTambahan' => $this->post('tugasTambahan'),
            'jumlahSKS' => $this->post('jumlahSKS'),
            'rata2' => $this->post('rata2'),
        ];
        
        $req =  $this->tb->createData($data);
        if( $req > 0)
        {
            $this->response([
                'status' => true,
                'id'=> $req,
                'message' => 'data baru telah ditambahkan'
            ], 201);
        }
        else 
        {
            $this->response([
                'status' => false,
                'message' => 'post failed'
            ], 400);
        }
    }

    public function  index_put()
    {
        $id = $this->put('id');
        $data = [
            'prodi' => $this->put('prodi'),
            'namaDosen' => $this->put('namaDosen'),
            'DTPS' => $this->put('DTPS'),
            'PSAkreditasi' => $this->put('PSAkreditasi'),
            'PSLainDalamPT' => $this->put('PSLainDalamPT'),
            'PSLainLuarPT' => $this->put('PSLainLuarPT'),
            'penelitian' => $this->put('penelitian'),
            'pkm' => $this->put('pkm'),
            'tugasTambahan' => $this->put('tugasTambahan'),
            'jumlahSKS' => $this->put('jumlahSKS'),
            'rata2' => $this->put('rata2'),
        ];
        
        if( $this->tb->updateData($data, $id) > 0)
        {
            $this->response([
                'status' => true,
                'id' =>$id,
                'message' => 'data has been updated'
            ], 200);
        }
        else 
        {
            $this->response([
                'status' => false,
                'id' =>$id,
                'message' => 'update nothing'
            ], 200);
        }
    }
}

